package utils

import (
  "log"
  "os"
  "path/filepath"
  "sync"
)

type IFileUtils interface {
  Open(path string) (*os.File, error)
  Close(file *os.File)
  OpenRoot(filename string) (*os.File, error)
}

var (
  basepath, _ = os.Getwd()
  Root        = filepath.Join(basepath)
)

type fileUtils struct {
}

var iFileUtils IFileUtils

func NewFileUtils() IFileUtils {
  lock := &sync.Mutex{}
  if iFileUtils == nil {
    lock.Lock()
    defer lock.Unlock()
    iFileUtils = &fileUtils{}
  }
  return iFileUtils
}

func (f fileUtils) Open(path string) (*os.File, error) {
  jsonFile, err := os.Open(path)
  if err != nil {
    log.Print(err)
    return nil, err
  }
  return jsonFile, nil
}

func (f fileUtils) Close(file *os.File) {
  err := file.Close()
  if err != nil {
    log.Print(err)
  }
}

func (f fileUtils) OpenRoot(filename string) (*os.File, error) {
  return f.Open(Root + "/" + filename)
}

func IsExists(filename string) bool {
  info, err := os.Stat(filename)
  if os.IsNotExist(err) {
    return false
  }
  return !info.IsDir()
}
