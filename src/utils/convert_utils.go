package utils

import (
  "encoding/json"
  "io"
  "log"
  "os"
)

type ConvertUtils[T any] struct {
}

func (c ConvertUtils[T]) ToBlockchain(file *os.File, object *T) error {
  byteValue, _ := io.ReadAll(file)
  err := json.Unmarshal(byteValue, object)
  if err != nil {
    log.Print(err)
    return err
  }
  return nil
}
