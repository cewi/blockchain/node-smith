package utils

import (
  "crypto/sha512"
  "encoding/hex"
  "sync"
)

type IHashUtils interface {
  Generate(value string) (string, []byte)
}

type hashUtils struct {
}

var iHashUtils IHashUtils

func NewHashUtils() IHashUtils {
  lock := &sync.Mutex{}
  if iHashUtils == nil {
    lock.Lock()
    defer lock.Unlock()
    iHashUtils = &hashUtils{}
  }
  return &hashUtils{}
}

func (r *hashUtils) Generate(value string) (string, []byte) {
  h := sha512.New()
  h.Write([]byte(value))
  hashed := h.Sum(nil)
  return hex.EncodeToString(hashed), hashed
}
