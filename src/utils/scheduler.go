package utils

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/go-co-op/gocron"
	"github.com/robfig/cron/v3"
)

func RunCronJobs(cron string, f func()) {
	go run(cron, f)
}

var isRunner = false

func run(cron string, f func()) {
	s := gocron.NewScheduler(time.UTC)
	s.Clear()
	_, err := s.Cron(cron).Do(
		func() {
			if !isRunner {
				isRunner = true
				f()
			} else {
				isRunner = false
				return
			}
		},
	)
	if err != nil {
		log.Println(err)
		return
	}
	s.StartBlocking()
}

var teste = 0

func runCronJobs() {
	// 2
	s := cron.New()
	// 3
	s.AddFunc(
		"*/1 * * * *", func() {
			teste++
			fmt.Print(" " + strconv.Itoa(teste))
		},
	)

	// 4
	s.Start()
}
