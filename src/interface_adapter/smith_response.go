package interface_adapter

type SmithResponse struct {
	Hash      string `bson:"_id"` // Hash da época atual exemplo: CreatedAt +  ChainID + HashBlockchain + PublicSignature
	MyAddress string `bson:"my_address"`
}
