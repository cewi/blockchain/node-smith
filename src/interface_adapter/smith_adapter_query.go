package interface_adapter

import (
	"context"
	"log"
	"os"
	"sync"

	"gitlab.com/cewi/blockchain/node-smith/src/entities"
	pb "gitlab.com/cewi/blockchain/node-smith/src/framework/proto_buffer"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ISmithAdapterQuery interface {
	CheckWinner(smith *entities.Smith) (*pb.SmithCheckResponse, error)
	SendFine(smith *entities.Smith)
	CreateNewBlock(smith *entities.Smith, signature string, blocks string)
	GetBalance(req *pb.SmithCheckRequest) (*pb.SmithBalanceResponse, error)
}

var iSmithAdapterQuery ISmithAdapterQuery

type smithAdapterQuery struct {
}

func NewSmithAdapterQuery() ISmithAdapterQuery {
	lock := &sync.Mutex{}
	if iSmithAdapterQuery == nil {
		lock.Lock()
		defer lock.Unlock()
		iSmithAdapterQuery = &smithAdapterQuery{}
	}
	return iSmithAdapterQuery
}

func (s *smithAdapterQuery) CheckWinner(smith *entities.Smith) (*pb.SmithCheckResponse, error) {

	connection := s.connectBlockchain()
	defer func(connection *grpc.ClientConn) {
		err := connection.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}(connection)

	client := pb.NewISmithQueryClient(connection)
	request := &pb.SmithCheckRequest{
		Hash: smith.Hash,
	}

	return client.CheckWinner(context.Background(), request)
}

func (s *smithAdapterQuery) CreateNewBlock(smith *entities.Smith, signature string, blocks string) {
	connection := s.connectBlockchain()
	defer func(connection *grpc.ClientConn) {
		err := connection.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}(connection)

	client := pb.NewIBlockchainCommandClient(connection)
	request := &pb.BlockchainRequest{
		HashSmith: smith.Hash,
		MyAddress: smith.MyAddress,
		Signature: signature,
		Blocks:    blocks,
	}

	_, err := client.CreateNewBlock(context.Background(), request)

	if err != nil {
		log.Println(err.Error())
		panic(err)
	}
}

func (s *smithAdapterQuery) SendFine(smith *entities.Smith) {

	connection := s.connectBlockchain()
	defer func(connection *grpc.ClientConn) {
		err := connection.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}(connection)

	client := pb.NewISmithCommandClient(connection)
	request := &pb.SmithFineRequest{
		Hash:      smith.Hash,
		MyAddress: smith.MyAddress,
	}
	_, errRes := client.SendFine(context.Background(), request)
	if errRes != nil {
		panic(errRes)
	}
}

func (s *smithAdapterQuery) GetBalance(req *pb.SmithCheckRequest) (*pb.SmithBalanceResponse, error) {
	connection := s.connectBlockchain()
	defer func(connection *grpc.ClientConn) {
		err := connection.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}(connection)

	client := pb.NewISmithQueryClient(connection)

	return client.GetBalance(context.Background(), req)
}

func (s *smithAdapterQuery) connectBlockchain() *grpc.ClientConn {
	host := os.Getenv("BLOCKCHAIN_GRPC_SERVER_HOST")
	if host == "" {
		panic("blockchain host not found")
	}
	port := os.Getenv("BLOCKCHAIN_GRPC_SERVER_PORT")
	if port == "" {
		panic("blockchain host not found")
	}

	connection, err := grpc.Dial(host+":"+port, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		panic(err)
	}

	return connection
}
