package use_case

import (
	"encoding/json"
	"errors"
	"log"
	"os"
	"sort"
	"sync"

	"gitlab.com/cewi/blockchain/node-smith/src/entities"
	pb "gitlab.com/cewi/blockchain/node-smith/src/framework/proto_buffer"
	"gitlab.com/cewi/blockchain/node-smith/src/interface_adapter"
	"gitlab.com/cewi/blockchain/node-smith/src/utils"
)

var Smith *entities.Smith

func Init() {
	filename := os.Getenv("SMITH_PATH_ROOT") + "/" + os.Getenv("SMITH_FILE_NAME")
	exists := utils.IsExists(filename)
	fileUtils := utils.NewFileUtils()

	if !exists {
		log.Println("file smith.json not found")
		return
	}

	file, err := fileUtils.Open(filename)
	if err != nil {
		log.Println(err.Error())
		return
	}

	smith := new(entities.Smith)
	_ = utils.ConvertUtils[*entities.Smith]{}.ToBlockchain(file, &smith)

	Smith = smith
	NewSmithInteractor()
}

type ISmithInteractor interface {
	CheckWinner()
	GetBalance(req *pb.SmithRequestEmpty) (*pb.SmithBalanceServerResponse, error)
}

var iSmithInteractor ISmithInteractor

type smithInteractor struct {
	iSmithAdapterQuery interface_adapter.ISmithAdapterQuery
}

func NewSmithInteractor() ISmithInteractor {
	lock := &sync.Mutex{}
	if iSmithInteractor == nil {
		lock.Lock()
		defer lock.Unlock()
		iSmithInteractor = &smithInteractor{
			iSmithAdapterQuery: interface_adapter.NewSmithAdapterQuery(),
		}
	}
	utils.RunCronJobs(
		"*/1 * * * *", func() {
			iSmithInteractor.CheckWinner()
		},
	)
	return iSmithInteractor
}

func (i *smithInteractor) CheckWinner() {

	res, errSmith := i.iSmithAdapterQuery.CheckWinner(Smith)

	if errSmith != nil {
		log.Println(errSmith.Error())
		panic(errSmith)
	}

	if res.Blocks == "" || res.Epoch == "" || res.Signature == "" {
		log.Println("block to validate not found ")
		return
	}

	signatureUtil := utils.NewSignatureUtil()
	blocksJson, errBase64 := signatureUtil.Base64ToString(res.Blocks)
	if errBase64 != nil {
		log.Println(errBase64.Error())
		panic(errBase64)
	}

	publicSignatureString, errSign := signatureUtil.Base64ToString(res.PublicSignature)
	if errSign != nil {
		log.Println(errSign.Error())
		panic(errSign)
	}

	publicKey, errConvert := signatureUtil.StringToPublicKey(publicSignatureString)
	if errConvert != nil {
		log.Println(errConvert.Error())
		panic(errConvert)
	}

	signatureString, errConvert2 := signatureUtil.Base64ToString(res.Signature)
	if errConvert2 != nil {
		log.Println(errConvert2.Error())
		panic(errConvert2)
	}

	pss := signatureUtil.VerifyPSS(publicKey, blocksJson, []byte(signatureString))
	if pss {
		var blocks []*entities.Block

		errJson := json.Unmarshal([]byte(blocksJson), &blocks)
		if errJson != nil {
			log.Println(errJson.Error())
			panic(errJson)
		}

		sort.Slice(
			blocks, func(i, j int) bool {
				return blocks[i].CreatedAt < blocks[j].CreatedAt
			},
		)

		if len(blocks) == 0 {
			log.Println("blocks not found")
			return
		}

		if errValidate := i.validateBlocks(blocks); errValidate != nil {
			log.Println(errValidate.Error())
			i.iSmithAdapterQuery.SendFine(Smith)
			panic(errValidate)
		} else {
			i.iSmithAdapterQuery.CreateNewBlock(Smith, res.Signature, res.Blocks)
			log.Println("new block sign")
			return
		}

	}
	return
}

func (i *smithInteractor) GetBalance(req *pb.SmithRequestEmpty) (*pb.SmithBalanceServerResponse, error) {
	res := &pb.SmithBalanceServerResponse{
		Status: "FAIL",
	}

	balanceRes, err1 := i.iSmithAdapterQuery.GetBalance(
		&pb.SmithCheckRequest{
			Hash:      Smith.Hash,
			MyAddress: Smith.MyAddress,
		},
	)
	if err1 != nil {
		res.Message = err1.Error()
		return res, err1
	}

	if balanceRes.Status == "FAIL" {
		res.Message = "there was a failure when fetching the balance sheet"
		return res, errors.New("there was a failure when fetching the balance sheet")
	}

	signatureUtil := utils.NewSignatureUtil()

	privateKeyBase64String, err3 := signatureUtil.Base64ToString(Smith.PrivateKeyBase64)
	if err3 != nil {
		res.Message = err3.Error()
		return res, err3
	}

	primaryKey, err4 := signatureUtil.StringToPrimaryKey(privateKeyBase64String)
	if err4 != nil {
		res.Message = err4.Error()
		return res, err4
	}

	balanceString, err5 := signatureUtil.Base64ToString(balanceRes.Balance)
	if err5 != nil {
		res.Message = err5.Error()
		return res, err5
	}

	balance, err6 := signatureUtil.Dencrypt(primaryKey, []byte(balanceString))
	if err6 != nil {
		res.Message = err6.Error()
		return res, err6
	}

	res.Status = "OK"
	res.Balance = balance

	return res, nil
}

func (i *smithInteractor) validateBlocks(blocks []*entities.Block) error {

	currBlockIdx := len(blocks) - 1
	prevBlockIdx := len(blocks) - 2
	for prevBlockIdx > 0 {
		currBlock := blocks[currBlockIdx]
		prevBlock := blocks[prevBlockIdx]

		// Check if the previous hash of the current block matches the hash of the previous block
		if currBlock.PrevHash != prevBlock.Hash {
			return errors.New("inconsistent hashes")
		}
		// Check if the timestamp of the current block is greater than the timestamp of the previous block
		if currBlock.CreatedAt <= prevBlock.CreatedAt {
			return errors.New("inconsistent timestamps")
		}
		oldBlock := blocks[prevBlockIdx]
		hashUtils := utils.NewHashUtils()

		prevHash, _ := hashUtils.Generate(currBlock.CreatedAt + oldBlock.Hash + currBlock.Smith + currBlock.HashEpoch)
		// Check if the previous hash of the previous block matches the expected value
		if prevHash != currBlock.Hash {
			return errors.New("inconsistent hash generation")
		}

		otherBlock := blocks[prevBlockIdx-1]
		oldPrevHash, _ := hashUtils.Generate(oldBlock.CreatedAt + otherBlock.Hash + oldBlock.Smith + oldBlock.HashEpoch)
		// Check if the previous hash of the previous block matches the expected value
		if oldPrevHash != oldBlock.Hash {
			return errors.New("inconsistent hash generation")
		}

		// Check if the validator smith  of the previous block matches the smith  of the current block
		if prevBlock.ValidatorSmith != currBlock.Smith {
			return errors.New("inconsistent smith ")
		}

		currBlockIdx--
		prevBlockIdx--
	}
	return nil
}
