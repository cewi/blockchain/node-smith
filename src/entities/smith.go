package entities

type Smith struct {
	Hash             string `json:"hash"`
	ChainID          string `json:"chain_id"`
	HashBlockchain   string `json:"hash_blockchain"`
	PrivateKey       string `json:"private_key"`
	PrivateKeyBase64 string `json:"private_key_base_64"`
	PublicKey        string `json:"public_key"`
	PublicKeyBase64  string `json:"public_key_base_64"`
	MyAddress        string `json:"my_address"`
}
