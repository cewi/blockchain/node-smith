package controller

import (
	"context"
	"sync"

	pb "gitlab.com/cewi/blockchain/node-smith/src/framework/proto_buffer"
	"gitlab.com/cewi/blockchain/node-smith/src/use_case"
)

type smithController struct {
	smithInteractor use_case.ISmithInteractor
}

var iSmithController pb.ISmithServer

func NewSmithController() pb.ISmithServer {
	mutex := &sync.Mutex{}

	if iSmithController == nil {
		mutex.Lock()
		defer mutex.Unlock()

		iSmithController = &smithController{
			smithInteractor: use_case.NewSmithInteractor(),
		}
	}

	return iSmithController
}

func (s *smithController) GetBalance(_ context.Context, req *pb.SmithRequestEmpty) (*pb.SmithBalanceServerResponse, error) {
	return s.smithInteractor.GetBalance(req)
}
