package application

import (
	"log"
	"net"
	"os"

	"gitlab.com/cewi/blockchain/node-smith/src/framework/controller"
	pb "gitlab.com/cewi/blockchain/node-smith/src/framework/proto_buffer"
	"gitlab.com/cewi/blockchain/node-smith/src/use_case"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	TCP = "tcp"
)

func Init() {
	use_case.Init()

	grpcServer := grpc.NewServer()

	reflection.Register(grpcServer)
	pb.RegisterISmithServer(grpcServer, controller.NewSmithController())

	port := os.Getenv("GRPC_PORT")

	listener, err := net.Listen(TCP, ":"+port)
	if err != nil {
		log.Fatalf(err.Error())
	}
	log.Printf("Node Smith, inicializada com sucesso na porta: %v\n", port)
	errs := grpcServer.Serve(listener)
	if errs != nil {
		panic(errs)
	}

}
