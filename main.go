package main

import (
	application "gitlab.com/cewi/blockchain/node-smith/src"
)

func main() {
	application.Init()
}
